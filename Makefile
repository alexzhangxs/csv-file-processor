#!make

.PHONY: build
build:
	go build -o file_processor .

.PHONY: build-docker-image
build-docker-image:
	docker build -t csv_file_processor .

.PHONY: start-docker-server
start-docker-server: start-docker-mysql
	docker-compose up -d file-processor

.PHONY: start-docker-mysql
start-docker-mysql:
	docker-compose up -d local-mysql

.PHONY: init-docker-mysql
init-docker-mysql: start-docker-mysql
	sleep 20
	docker exec -it local-mysql bash -c 'mysql -uroot -proot -e "CREATE DATABASE IF NOT EXISTS file_processor"'