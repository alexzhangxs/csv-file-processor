package controller

import (
	"csv-file-processor/server/request"
	"csv-file-processor/server/response"
	"csv-file-processor/server/service"

	"github.com/kataras/iris/v12"
)

type DataController struct {
	Ctx iris.Context
}

func (c *DataController) Get(query request.DataFilterRequest) {
	list, meta, err := service.NewDataService(c.Ctx).Search(&query)
	resp := response.NewApiResponseWithMeta(c.Ctx)
	if err != nil {
		resp.RespondError(err)
		return
	}
	resp.RespondSuccessWithMeta(list, meta)
}

func (c *DataController) Post(ctx iris.Context) {
	err := service.NewDataService(c.Ctx).ProcessUploadedCsv(ctx)
	resp := response.NewApiResponse(ctx)
	if err != nil {
		resp.RespondError(err)
		return
	}
	resp.RespondCreated("All data successfully saved!")
}
