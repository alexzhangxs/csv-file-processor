package repository

import (
	"csv-file-processor/config"
	"csv-file-processor/dto"
	"log"
	"os"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var db *gorm.DB

func init() {
	db = GetDbConnection()
	migrate()
}

func GetDbConnection() *gorm.DB {
	dbConfig := config.DBConfig
	dsn := dbConfig.UserName + ":" + dbConfig.Password + "@tcp(" + dbConfig.HostName + ":" + dbConfig.Port + ")/" + dbConfig.DatabaseName + "?charset=utf8mb4&parseTime=True"
	dbConnection, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		Logger: logger.New(log.New(os.Stdout, "\r\n", log.LstdFlags), logger.Config{
			SlowThreshold:             2 * time.Second,
			LogLevel:                  logger.Error,
			IgnoreRecordNotFoundError: false,
			Colorful:                  true,
		}),
	})
	if err != nil {
		panic(err.Error())
	}
	return dbConnection
}

func migrate() {
	db.Migrator().AutoMigrate(&dto.PurchaseData{})
}
