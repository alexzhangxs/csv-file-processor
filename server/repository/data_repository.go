package repository

import (
	"csv-file-processor/dto"
	"csv-file-processor/helpers"
	"csv-file-processor/server/request"

	"github.com/kataras/iris/v12"
	"gorm.io/gorm"
)

type DataRepository struct {
	Ctx iris.Context
	Tdb *helpers.TransDb
}

func NewDataRepository(ctx iris.Context, tdb *helpers.TransDb) *DataRepository {
	return &DataRepository{
		Ctx: ctx,
		Tdb: tdb,
	}
}

func (r *DataRepository) getDB() *gorm.DB {
	if r.Tdb != nil && (*r.Tdb) != nil {
		return *r.Tdb
	}
	return db
}

func (r *DataRepository) Search(request *request.DataFilterRequest) ([]*dto.PurchaseData, *Paginator, error) {
	rdb := r.getDB()
	if request.User != nil {
		rdb = rdb.Where("user = ?", *request.User)
	}
	if request.Gender != nil {
		rdb = rdb.Where("gender = ?", *request.Gender)
	}
	if request.Store != nil {
		rdb = rdb.Where("store = ?", *request.Store)
	}
	paginationParam := newPaginationParam(&request.PaginationRequest)

	list := []*dto.PurchaseData{}
	paginator, err := searchWithPagination(rdb, paginationParam, &list)
	return list, paginator, err
}

func (r *DataRepository) BatchCreate(list []*dto.PurchaseData) error {
	return r.getDB().CreateInBatches(list, 1000).Error
}
