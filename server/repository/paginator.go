package repository

import (
	"context"
	"csv-file-processor/helpers"
	"csv-file-processor/server/request"

	"gorm.io/gorm"
)

type PaginationParam struct {
	Page    int
	Limit   int
	OrderBy []string
}

func newPaginationParam(r *request.PaginationRequest) *PaginationParam {
	return &PaginationParam{
		Page:    r.CurrentPage,
		Limit:   r.PageSize,
		OrderBy: r.OrderBy,
	}
}

type Paginator struct {
	TotalRecord int `json:"total_record"`
	TotalPage   int `json:"total_page"`
	Offset      int `json:"offset"`
	Limit       int `json:"limit"`
	From        int `json:"from"`
	To          int `json:"to"`
	Page        int `json:"page"`
	PrevPage    int `json:"prev_page"`
	NextPage    int `json:"next_page"`
}

func searchWithPagination(database *gorm.DB, param *PaginationParam, dataList interface{}) (paginator *Paginator, err error) {
	getCountChan := make(chan int64, 1)

	ctxDB := database.WithContext(context.Background())
	go getTotalCount(ctxDB, dataList, getCountChan)

	if param.Page < 1 {
		param.Page = 1
	}
	if param.Limit <= 0 {
		param.Limit = helpers.DefaultPageSize
	}
	offset := (param.Page - 1) * param.Limit
	database = database.Limit(param.Limit).Offset(offset)
	for _, order := range param.OrderBy {
		database.Order(order)
	}

	if findErr := database.Find(dataList).Error; findErr != nil && findErr != gorm.ErrRecordNotFound {
		err = findErr
		return
	}

	count := <-getCountChan
	paginator = &Paginator{
		TotalRecord: int(count),
		TotalPage:   1,
		Offset:      offset,
		From:        offset + 1,
		To:          offset + param.Limit,
		Limit:       param.Limit,
		Page:        param.Page,
		PrevPage:    param.Page - 1,
		NextPage:    param.Page + 1,
	}
	if paginator.TotalRecord > 0 {
		paginator.TotalPage = (paginator.TotalRecord-1)/param.Limit + 1
	}
	if paginator.To > paginator.TotalRecord {
		paginator.To = paginator.TotalRecord
	}
	if paginator.PrevPage <= 0 {
		paginator.PrevPage = 1
	}
	if paginator.NextPage > paginator.TotalPage {
		paginator.NextPage = paginator.TotalPage
	}

	return
}

func getTotalCount(database *gorm.DB, dataList interface{}, countChan chan<- int64) {
	var count int64
	database.Model(dataList).Count(&count)
	countChan <- count
}
