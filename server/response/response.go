package response

import (
	"csv-file-processor/server/repository"

	"github.com/kataras/iris/v12"
)

type ApiResponse struct {
	Ctx         iris.Context `json:"-"`
	Description string       `json:"description,omitempty"`
	Data        interface{}  `json:"data"`
}

func NewApiResponse(ctx iris.Context) *ApiResponse {
	return &ApiResponse{
		Ctx: ctx,
	}
}

type ApiResponseWithMeta struct {
	*ApiResponse
	Meta *repository.Paginator `json:"meta,omitempty"`
}

func NewApiResponseWithMeta(ctx iris.Context) *ApiResponseWithMeta {
	return &ApiResponseWithMeta{
		ApiResponse: &ApiResponse{
			Ctx: ctx,
		},
	}
}

type ApiError struct {
	Code        int    `json:"code"`
	Description string `json:"description"`
}

func NewApiError(err error, defaultErrCode ...int) *ApiError {
	if apiErr, ok := err.(*ApiError); ok {
		return apiErr
	}
	errCode := iris.StatusInternalServerError
	if len(defaultErrCode) > 0 {
		errCode = defaultErrCode[0]
	}
	return &ApiError{
		Code:        errCode,
		Description: err.Error(),
	}
}

func (e *ApiError) Error() string {
	return e.Description
}

func (r *ApiResponse) RespondSuccess(data interface{}) {
	r.Data = data
	r.Ctx.StatusCode(iris.StatusOK)
	r.Ctx.JSON(r)
}

func (r *ApiResponse) RespondCreated(desc string) {
	r.Description = desc
	r.Ctx.StatusCode(iris.StatusCreated)
	r.Ctx.JSON(r)
}

func (r *ApiResponseWithMeta) RespondSuccessWithMeta(data interface{}, meta *repository.Paginator) {
	r.Meta = meta
	r.Data = data
	r.Ctx.StatusCode(iris.StatusOK)
	r.Ctx.JSON(r)
}

func (r *ApiResponse) RespondError(err error, statusCode ...int) {
	apiErr, ok := err.(*ApiError)
	if !ok {
		apiErr = NewApiError(err, statusCode...)
	}
	r.Data = apiErr
	r.Ctx.StatusCode(apiErr.Code)
	r.Ctx.JSON(r)
}
