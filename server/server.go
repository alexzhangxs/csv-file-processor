package server

import (
	"csv-file-processor/server/controller"

	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/mvc"
)

func Start() {
	app := iris.New()
	mvcApp := mvc.New(app.Party("/"))
	mvcApp.Party("/data").Handle(new(controller.DataController))
	app.Listen(":8082")
}
