package request

type PaginationRequest struct {
	CurrentPage int      `url:"current_page"`
	PageSize    int      `url:"page_size"`
	OrderBy     []string `url:"order_by"`
}

type DataFilterRequest struct {
	User   *string `url:"user"`
	Gender *string `url:"gender"`
	Store  *string `url:"store"`

	PaginationRequest
}
