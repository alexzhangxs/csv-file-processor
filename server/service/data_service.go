package service

import (
	"csv-file-processor/dto"
	"csv-file-processor/helpers"
	"csv-file-processor/server/repository"
	"csv-file-processor/server/request"
	"csv-file-processor/server/response"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/kataras/iris/v12"
)

type DataService struct {
	Ctx iris.Context
}

func NewDataService(ctx iris.Context) *DataService {
	return &DataService{
		Ctx: ctx,
	}
}

func (s *DataService) Search(query *request.DataFilterRequest) ([]*dto.PurchaseData, *repository.Paginator, error) {
	list, meta, err := repository.NewDataRepository(s.Ctx, nil).Search(query)
	if err != nil {
		err = response.NewApiError(err)
	}
	return list, meta, err
}

func (s *DataService) ProcessUploadedCsv(ctx iris.Context) error {
	timeStart := time.Now()
	defer func() {
		helpers.Logf("ProcessUploadedCsv done. total duration: %f sec", time.Now().Sub(timeStart).Seconds())
	}()

	fileHeader, err := s.getUploadedFileHeader(ctx)
	if err != nil {
		return response.NewApiError(err, iris.StatusBadRequest)
	}

	timeStep1 := time.Now()
	helpers.Logf("STEP 1: get uploaded file done. header name: %s, size: %d, duration: %f sec", fileHeader.Filename, fileHeader.Size, timeStep1.Sub(timeStart).Seconds())

	pdList, err := s.readPurchaseDataFromFile(fileHeader)
	if err != nil {
		return response.NewApiError(err, iris.StatusBadRequest)
	}

	timeStep2 := time.Now()
	helpers.Logf("STEP 2: read purchase data done. list length: %d, duration: %f sec", len(pdList), timeStep2.Sub(timeStep1).Seconds())

	if err := s.savePurchaseDataList(pdList); err != nil {
		return response.NewApiError(err, iris.StatusInternalServerError)
	}

	timeStep3 := time.Now()
	helpers.Logf("STEP 3: save data to DB done. duration: %f sec", timeStep3.Sub(timeStep2).Seconds())
	return nil
}

func (s *DataService) getUploadedFileHeader(ctx iris.Context) (fh *multipart.FileHeader, err error) {
	maxSize := ctx.Application().ConfigurationReadOnly().GetPostMaxMemory()
	err = ctx.Request().ParseMultipartForm(maxSize)
	if err != nil {
		err = fmt.Errorf("Failed to parse multipart form: %v", err)
		return
	}
	multipartForm := ctx.Request().MultipartForm
	if multipartForm == nil {
		err = errors.New("Uploaded file not found")
		return
	}
	fileHeaders := []*multipart.FileHeader{}
	for _, fhGroup := range multipartForm.File {
		for _, fh := range fhGroup {
			fileHeaders = append(fileHeaders, fh)
		}
	}
	if len(fileHeaders) == 0 {
		err = errors.New("Uploaded file not found")
		return
	}
	if len(fileHeaders) > 1 {
		err = errors.New("Cannot process multiple files")
		return
	}
	fh = fileHeaders[0]
	if fnParts := strings.Split(fh.Filename, "."); len(fnParts) <= 1 || strings.ToLower(fnParts[len(fnParts)-1]) != "csv" {
		err = errors.New("File extension not supported")
		return
	}
	return
}

func (s *DataService) readPurchaseDataFromFile(fh *multipart.FileHeader) ([]*dto.PurchaseData, error) {
	file, err := fh.Open()
	if err != nil {
		return nil, response.NewApiError(err, iris.StatusInternalServerError)
	}
	defer file.Close()

	csvReader := csv.NewReader(file)
	return s.readPurchaseDataWithCsvReader(csvReader)
}

type purchaseDataMessage struct {
	data      *dto.PurchaseData
	err       error
	completed bool
}

func (s *DataService) readPurchaseDataWithCsvReader(csvReader *csv.Reader) ([]*dto.PurchaseData, error) {
	purchaseDataList := []*dto.PurchaseData{}

	msgChan := s.getPurchaseDataChanWithCsvReader(csvReader)
	for {
		msg := <-msgChan
		switch {
		case msg.completed:
			return purchaseDataList, nil
		case msg.err != nil:
			return nil, msg.err
		case msg.data != nil:
			purchaseDataList = append(purchaseDataList, msg.data)
		}
	}
}

func (s *DataService) getPurchaseDataChanWithCsvReader(csvReader *csv.Reader) (msgChan chan purchaseDataMessage) {
	msgChan = make(chan purchaseDataMessage, 1000)

	// always ignore the first line
	if _, err := csvReader.Read(); err == io.EOF {
		msgChan <- purchaseDataMessage{completed: true}
		return
	}

	var wg sync.WaitGroup

	dispatchDone := make(chan bool, 1)
	go s.dispatchGetDataTasks(csvReader, msgChan, dispatchDone, &wg)

	go func() {
		// waiting for all tasks dispatched
		<-dispatchDone
		// waiting for all dispatched tasks finished
		wg.Wait()
		// confirm all tasks completed
		msgChan <- purchaseDataMessage{completed: true}
	}()
	return
}

func (s *DataService) dispatchGetDataTasks(csvReader *csv.Reader, msgChan chan purchaseDataMessage, dispatchDone chan<- bool, wg *sync.WaitGroup) {
	defer func() {
		dispatchDone <- true
	}()

	stopDispatch := make(chan bool, 1)
	runningCounterChan := make(chan struct{}, 1000) // the size of this chan will be the maximum number of concurrently running tasks
	var lineNo int = 1
	for {
		lineNo++

		record, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			msgChan <- purchaseDataMessage{
				err: response.NewApiError(err, iris.StatusBadRequest),
			}
			return
		}

		// do not continue to dispatch new tasks if one of existing tasks returns an error
		select {
		case <-stopDispatch:
			return
		default:
		}

		// dispatch a goroutine task to validate & decode line to PurchaseData and add to list
		runningCounterChan <- struct{}{} // count of running tasks +1, will be blocked here and waiting for any running task to be finished if channel is full
		wg.Add(1)
		go func(lineNo int, record []string, wg *sync.WaitGroup, msgChan chan purchaseDataMessage, runningCounterChan chan struct{}, stopDispatch chan bool) {
			defer func() {
				<-runningCounterChan // count of running tasks -1
				wg.Done()
			}()
			data, err := s.getPurchaseDataFromCsvLine(lineNo, record)
			if err != nil {
				stopDispatch <- true
				msgChan <- purchaseDataMessage{
					err: response.NewApiError(err, iris.StatusBadRequest),
				}
				return
			}
			msgChan <- purchaseDataMessage{data: data}
		}(lineNo, record, wg, msgChan, runningCounterChan, stopDispatch)
	}
}

func (s *DataService) getPurchaseDataFromCsvLine(lineNo int, record []string) (*dto.PurchaseData, error) {
	purchaseData := &dto.PurchaseData{}
	if len(record) != 6 {
		return nil, fmt.Errorf("Line: %d: incorrect column size", lineNo)
	}

	// validate user
	if len(record[0]) > 255 {
		return nil, fmt.Errorf("Line: %d: user length exceeding limit", lineNo)
	}
	purchaseData.User = record[0]

	// validate age
	if len(record[1]) != 0 {
		if age, err := strconv.ParseInt(record[1], 10, 64); err != nil {
			return nil, fmt.Errorf("Line: %d: age is not a valid number", lineNo)
		} else {
			purchaseData.Age = &age
		}
	}

	// validate store
	if len(record[2]) > 255 {
		return nil, fmt.Errorf("Line: %d: store length exceeding limit", lineNo)
	}
	purchaseData.Store = record[2]

	// validate gender
	if len(record[3]) > 255 {
		return nil, fmt.Errorf("Line: %d: gender length exceeding limit", lineNo)
	}
	purchaseData.Gender = record[3]

	// validate amount
	if len(record[4]) > 0 {
		if amount, err := strconv.ParseFloat(record[4], 64); err != nil {
			return nil, fmt.Errorf("Line: %d: amount is not a valid number", lineNo)
		} else {
			purchaseData.Amount = &amount
		}
	}

	// validate purchase date
	if len(record[5]) > 0 {
		if t, err := helpers.ParseTime(record[5]); err != nil {
			return nil, fmt.Errorf("Line: %d: purchase date is invalid: %v", lineNo, err.Error())
		} else {
			purchaseData.PurchaseDate = t
		}
	}

	return purchaseData, nil
}

func (s *DataService) savePurchaseDataList(pdList []*dto.PurchaseData) error {
	return repository.NewDataRepository(s.Ctx, nil).BatchCreate(pdList)
}
