package service

import (
	"bytes"
	"encoding/csv"
	"fmt"
	"os"
	"strings"
	"testing"
)

var testDataServ *DataService

func TestGetPurchaseDataFromCsvLine(t *testing.T) {
	type testingCase struct {
		record    []string
		expectErr bool
	}
	testingCases := []testingCase{
		{
			record:    []string{"Jack", "30", "North 1", "male", "5000.89", "2022-01-31T11:01:42Z"}, // normal case
			expectErr: false,
		},
		{
			record:    []string{"", "", "", "", "", ""}, // allow empty for all columns
			expectErr: false,
		},
		{
			record:    []string{"123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345", "", "", "", "", ""}, // allow string with length <= 255 for user
			expectErr: false,
		},
		{
			record:    []string{"1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456", "", "", "", "", ""}, // disallow string with length >= 256 for user
			expectErr: true,
		},
		{
			record:    []string{"", "-1", "", "", "", ""}, // allow negative value for age
			expectErr: false,
		},
		{
			record:    []string{"", "1.2", "", "", "", ""}, // disallow noninteger for age
			expectErr: true,
		},
		{
			record:    []string{"", "1abc", "", "", "", ""}, // disallow any NaN for age
			expectErr: true,
		},
		{
			record:    []string{"", "", "", "", "-1.345", ""}, // allow negative value for amount
			expectErr: false,
		},
		{
			record:    []string{"", "", "", "", "1e-3", ""}, // allow scientific notation for amount
			expectErr: false,
		},
		{
			record:    []string{"", "", "", "", "abc", ""}, // disallow any NaN for amount
			expectErr: true,
		},
		{
			record:    []string{"", "", "", "", "", "2022-01-31T11:01:42+08:00"}, // allow datetime format like this
			expectErr: false,
		},
		{
			record:    []string{"", "", "", "", "", "2022-01-31 11:01:42"}, // allow datetime format like this
			expectErr: false,
		},
		{
			record:    []string{"", "", "", "", "", "Feb 27, 2019"}, // allow date format like this
			expectErr: false,
		},
		{
			record:    []string{"", "", "", "", "", "2018-12-31"}, // allow date format like this
			expectErr: false,
		},
		{
			record:    []string{"", "", "", "", "", "2018-12-32"}, // disallow invalid date
			expectErr: true,
		},
	}
	for i, c := range testingCases {
		_, e := testDataServ.getPurchaseDataFromCsvLine(i, c.record)
		if e == nil && c.expectErr {
			t.Errorf("Error for line %d record: (%s): error expected but succeeded", i, strings.Join(c.record, ","))
		}
		if e != nil && !c.expectErr {
			t.Errorf("Error for line %d record: (%s): error not expected but returned: %v", i, strings.Join(c.record, ","), e)
		}
	}
}

func TestReadPurchaseDataWithCsvReader(t *testing.T) {
	recordLen := 50

	buffer := new(bytes.Buffer)
	csvWriter := csv.NewWriter(buffer)
	intToStr := func(i int) string {
		return fmt.Sprintf("%d", i)
	}
	for i := 0; i <= recordLen; i++ {
		csvWriter.Write([]string{intToStr(i), "", "", "", "", ""})
	}
	csvWriter.Flush()
	if err := csvWriter.Error(); err != nil {
		t.Errorf("Unexpected error from csvWriter: %v", err)
		return
	}

	csvReader := csv.NewReader(buffer)
	dataList, err := testDataServ.readPurchaseDataWithCsvReader(csvReader)
	if err != nil {
		t.Errorf("Unexpected error from func readPurchaseDataWithCsvReader: %v", err)
		return
	}
	if dataLen := len(dataList); dataLen != recordLen {
		t.Errorf("Unexpected data length from func readPurchaseDataWithCsvReader: %d", dataLen)
	}
	userMap := map[string]bool{}
	for i := 1; i <= recordLen; i++ {
		userMap[intToStr(i)] = false
	}
	for _, data := range dataList {
		v, exists := userMap[data.User]
		if !exists {
			t.Errorf("Unexpected user %s returned from result", data.User)
		}
		if v {
			t.Errorf("Unexpected duplicated user %s returned", data.User)
		}
		userMap[data.User] = true
	}
	for k, v := range userMap {
		if !v {
			t.Errorf("User %s not returned", k)
		}
	}
}

func TestReadPurchaseDataWithCsvReaderWithError(t *testing.T) {
	recordLen := 50

	buffer := new(bytes.Buffer)
	csvWriter := csv.NewWriter(buffer)
	intToStr := func(i int) string {
		return fmt.Sprintf("%d", i)
	}
	for i := 0; i <= recordLen; i++ {
		if i == 30 {
			csvWriter.Write([]string{intToStr(i), "abc", "", "", "", ""})
		} else {
			csvWriter.Write([]string{intToStr(i), "", "", "", "", ""})
		}
	}
	csvWriter.Flush()
	if err := csvWriter.Error(); err != nil {
		t.Errorf("Unexpected error from csvWriter: %v", err)
		return
	}

	csvReader := csv.NewReader(buffer)
	dataList, err := testDataServ.readPurchaseDataWithCsvReader(csvReader)
	if err == nil {
		t.Errorf("Expected an error, but succeeded with data length: %d", len(dataList))
	}
	if err.Error() != "Line: 31: age is not a valid number" {
		t.Errorf("Unexpected error message: %v", err)
	}
}

func TestReadEmptyPurchaseDataWithCsvReader(t *testing.T) {
	buffer := new(bytes.Buffer)
	csvReader := csv.NewReader(buffer)
	dataList, err := testDataServ.readPurchaseDataWithCsvReader(csvReader)
	if err != nil {
		t.Errorf("Unexpected error from func readPurchaseDataWithCsvReader: %v", err)
		return
	}
	if len(dataList) > 0 {
		t.Errorf("Expected empty dataList, but returned %+v", dataList)
		return
	}
}

func TestMain(m *testing.M) {
	testDataServ = NewDataService(nil)
	os.Exit(m.Run())
}
