package dto

import "time"

type PurchaseData struct {
	Id           int64      `json:"id"`
	User         string     `json:"user" gorm:"type:varchar(255);index:idx_user"`
	Age          *int64     `json:"age"`
	Store        string     `json:"store" gorm:"type:varchar(255);index:idx_store"`
	Gender       string     `json:"gender" gorm:"type:varchar(255);index:idx_gender"`
	Amount       *float64   `json:"amount"`
	PurchaseDate *time.Time `json:"purchase_date"`
}
