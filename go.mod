module csv-file-processor

go 1.16

require (
	github.com/kataras/iris/v12 v12.2.0-alpha5.0.20220122232131-4899fe95f47d
	gorm.io/driver/mysql v1.2.3
	gorm.io/gorm v1.22.5
)
