package config

import "os"

type DatabaseConfig struct {
	HostName     string
	Port         string
	DatabaseName string
	UserName     string
	Password     string
}

var (
	DBConfig *DatabaseConfig
	LogOff   bool
)

func init() {
	DBConfig = &DatabaseConfig{
		HostName:     "localhost",
		Port:         "3306",
		DatabaseName: "file_processor",
		UserName:     "root",
		Password:     "root",
	}
	LogOff = false

	if env := os.Getenv("DBHostName"); len(env) > 0 {
		DBConfig.HostName = env
	}
	if env := os.Getenv("DBPort"); len(env) > 0 {
		DBConfig.Port = env
	}
	if env := os.Getenv("DBName"); len(env) > 0 {
		DBConfig.DatabaseName = env
	}
	if env := os.Getenv("DBUserName"); len(env) > 0 {
		DBConfig.UserName = env
	}
	if env := os.Getenv("DBPassword"); len(env) > 0 {
		DBConfig.Password = env
	}

	if os.Getenv("LOGOFF") == "true" {
		LogOff = true
	}
}
