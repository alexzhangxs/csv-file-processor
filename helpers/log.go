package helpers

import (
	"csv-file-processor/config"
	"log"
)

func Logf(format string, v ...interface{}) {
	if !config.LogOff {
		log.Printf(format, v...)
	}
}
