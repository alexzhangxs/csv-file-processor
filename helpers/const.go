package helpers

import "gorm.io/gorm"

type TransDb *gorm.DB

const DefaultPageSize = 10
