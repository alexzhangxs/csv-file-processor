package helpers

import (
	"fmt"
	"time"
)

var supportedLayouts = []string{
	time.RFC3339,
	time.RFC3339Nano,
	"2006-01-02 15:04:05",
	"2006-01-02 15:04:05 Z0700",
	"2006-01-02 15:04:05 Z0700 MST",
	"2006-01-02 15:04:05 Z07:00",
	"2006-01-02 15:04:05 Z07:00 MST",
	time.RFC822,
	time.RFC822Z,
	"2006-01-02",
	"02-01-2006",
	"Jan 02, 2006",
}

func ParseTime(str string) (*time.Time, error) {
	for _, layout := range supportedLayouts {
		if t, e := time.Parse(layout, str); e == nil {
			return &t, nil
		}
	}
	return nil, fmt.Errorf("Time format of string %v not supported", str)
}
