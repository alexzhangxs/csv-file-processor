# CSV File Processor

A demo HTTP server that could read purchase data of cup seller store from an uploaded csv file and save them to DB, and also provide an API for user to search saved data as well.

### Setups

#### Prerequisite

- docker & docker-compose
  _\* for running a MySQL server or the File Processor server in docker container(s)_
- go (v1.16 preferred)
  _\* for running the File Processor server in host machine_
- MySQL (5.7 preferred)
  _\* if you prefer not to run a docker container for Database_

#### MySQL Database

- If you already have a MySQL server available to use, you may create a database named "file_processor" (or any other name you like). Then you have to specify the following [environment variables](#envs) on starting the File Processor server

- If you do not have a MySQL server, you can create and initialise one running in a docker container by running command:
  `make init-docker-mysql`
  This command could also help to create database "file_processor" if not exists.

  > Once initialised, to start docker MySQL afterwards, you can run command
  > `make start-docker-mysql`

#### File Processor Server

##### Environment Variables <div id="envs"></div>

- `DBHostName`: Host name or IP address of MySQL server, default "localhost"
- `DBPort`: Port of MySQL server, default "3306"
- `DBName`: Name of Database, default "file_processor"
- `DBUserName`: Username of Database, default "root"
- `DBPassword`: Password of Database, default "root"
- `LOGOFF`: If set to "true", this would turn off stdout log for [POST /data](#post_data_api) API, default "false" or ""

##### Start Commands

- [_Option 1_] To directly start on local host machine, just run command
  `go run .`

  > If you have your own MySQL, you need to add the [environment variables](#envs):
  > `DBHostName=[Host Name] DBPort=[Port] [... Other Variables] go run .`

- [_Option 2_] To build an executive file on local host machine, you can run command:
  `make build`

  > You may want to add [environment variables](#envs):
  > `DBHostName=[Host Name] DBPort=[Port] [... Other Variables] make build`

  This would build an executable file named "file_processor". Then you may run below command to start:
  `./file_processor`

- [_Option 3_] To start on a docker container, run
  `make start-docker-server`

  Or you may just need to build the docker image:
  `make build-docker-image`

### EndPoint Details

#### `POST /data` <div id="post_data_api"></div>

**Request Body** (format: multipart/form-data)

- Key name: "file" (or any string you like)
- File: csv file with format requirements as below
  File sample:

  > USER,AGE,STORE,GENDER,AMOUNT,PURCHASE_DATE
  > Peter,31,central,male,215.10,2020-07-19T02:36:56Z
  > Krysty,25,south,female,1500.00,2021-05-10T21:36:40Z

  Columns explanation:
  | Column Name | Type | Validation Requirement |
  | - | - | - |
  | USER | string | Must be no longer than 255; can be empty |
  | AGE | int | Must be an integer if not empty; can be negative or empty |
  | STORE | string | Must be no longer than 255; can be empty |
  | GENDER | string | Must be no longer than 255; can be empty |
  | AMOUNT | double | Must be a number if not empty; can be negative or empty |
  | PURCHASE_DATE | datetime | Must match one of [supported datetime layouts](https://gitlab.com/alexzhangxs/csv-file-processor/-/blob/master/helpers/utils.go) if not empty; can be empty |

  Other notitifications:

  - There should be only one file uploaded for each API call, otherwise an error would be returned
  - The first line of csv file would be ignored, therefore it's always good to keep the header.

- **Response** (format: JSON)

  - Success
    Status Code: 201 Created
    Body

        {
            "description": "All data successfully saved!",
            "data": null
        }

  - Failure
    Status Code: 400 Bad Request or 500 Internal Server Error
    Body

        {
            "data": {
                "code": [Status Code],
                "description": "[Error Description]"
            }
        }

#### `GET /data?[key]=[value]&...` <div id="get_data_api"></div>

**Request**

- URL Params
  | Key Name | Valid Type | Description |
  | - | - | - |
  | user | string | Search data by user |
  | gender | string | Search data by gender |
  | store | string | Search data by store |
  | current_page | int | Set page number to be searched. Default is 1. Invalid if less than 1 or larger than total page number. |
  | page_size | int | Set maximum number of records each page contains. Default is 1. Invalid if less than 1. |
  | order_by | string array | Defines how searched data are ordered. For example, by defining "order_by=store&order_by=age desc", the search data will at first be ordered by ascending alphabetical order of "store", then by descending order of "age". Default is "id" ascending order. |

**Response**

- Success
  Status Code: 200 OK
  Body

      {
          "data": [
              {
                  "id": [id],
                  "user": "[user]",
                  "age": [age],
                  "store": "[store]",
                  "gender": "[gender]",
                  "amount": [amount],
                  "purchase_date": "[purchase_date]"
              },
              ... [other users]
          ]
          "meta": {
              "total_record": [total_record],
              "total_page": [total_page],
              "offset": [offset],
              "limit": [limit],
              "from": [from],
              "to": [to],
              "page": [page],
              "prev_page": [prev_page],
              "next_page": [next_page]
          }
      }

- Failure
  Status Code: 400 Bad Request or 500 Internal Server Error
  Body

      {
          "data": {
              "code": [Status Code],
              "description": "[Error Description]"
          }
      }
