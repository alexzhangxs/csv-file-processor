package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"time"
)

type csvLine struct {
	lineNo int
	record []string
}

var minTime int64 = time.Date(2010, 1, 1, 0, 0, 0, 0, time.Local).Unix()
var maxTime int64 = time.Date(2022, 1, 1, 0, 0, 0, 0, time.Local).Unix()

func newNormalRandomCsvLine(lineNo int) *csvLine {
	gender := "male"
	if rand.Intn(2) == 1 {
		gender = "female"
	}
	randTime := time.Unix(rand.Int63n(maxTime-minTime)+minTime, 0)
	return &csvLine{
		lineNo: lineNo,
		record: []string{
			fmt.Sprintf("User %d", rand.Intn(1000)),
			fmt.Sprintf("%d", rand.Intn(100)),
			fmt.Sprintf("Store %d", rand.Intn(1000)),
			gender,
			fmt.Sprintf("%f", rand.Float64()*10000),
			randTime.String(),
		},
	}
}

func generateRandomSampleCsvFile(fileName string, totalLines int, errLines ...*csvLine) error {
	dir, err := os.Getwd()
	if err != nil {
		return err
	}
	dir = filepath.Join(dir, "output")
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		if err := os.Mkdir(dir, 0777); err != nil {
			return err
		}
	}
	filePath := filepath.Join(dir, fileName)
	file, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, os.FileMode(0777))
	if err != nil {
		return err
	}
	defer file.Close()

	errLineMapping := map[int]*csvLine{}
	for _, line := range errLines {
		errLineMapping[line.lineNo] = line
	}

	csvWriter := csv.NewWriter(file)
	csvWriter.Write([]string{"USER", "AGE", "STORE", "GENDER", "AMOUNT", "PURCHASE_DATE"})
	for i := 1; i <= totalLines; i++ {
		if errLine, found := errLineMapping[i]; found {
			csvWriter.Write(errLine.record)
		} else {
			csvWriter.Write(newNormalRandomCsvLine(i).record)
		}
	}
	csvWriter.Flush()

	return csvWriter.Error()
}

func main() {
	if err := generateRandomSampleCsvFile("sample1.csv", 1000); err != nil {
		log.Fatalln(err)
	}
	if err := generateRandomSampleCsvFile("sample2.csv", 1000000); err != nil {
		log.Fatalln(err)
	}
	if err := generateRandomSampleCsvFile("sample3.csv", 15000000); err != nil {
		log.Fatalln(err)
	}
	if err := generateRandomSampleCsvFile(
		"sample4.csv",
		15000000,
		&csvLine{
			lineNo: 10000000,
			record: []string{
				"IncorrectUser1",
				"NAN",
				"",
				"",
				"NAN",
				"",
			},
		},
		&csvLine{
			lineNo: 10010000,
			record: []string{
				"IncorrectUser2",
				"",
				"",
				"",
				"",
				"Incorrect time",
			},
		},
	); err != nil {
		log.Fatalln(err)
	}
}
