FROM golang:1.16.7-alpine3.14
RUN apk add --no-cache bash

ADD . /app/src
WORKDIR /app/src
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /app/server main.go
RUN rm -rf /app/src

WORKDIR /app
RUN chmod +x server

ENTRYPOINT [ "./server" ]
